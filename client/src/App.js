import React, { Component } from 'react';
import './App.css';
import Header from './Components/Header';

class App extends Component {

  constructor(props){
    super(props);
    this.state = {
      resumeData: {}
    }; 
  }

  getMetaData() {
    fetch('/api/v1')
    .then(res => res.json())
    .then(resumeData => this.setState({resumeData}, () => console.log('Api fetched..', resumeData)));
  }

  componentDidMount() {
    this.getMetaData();
  }
  
  render() {  
  return (    
    <div className="App">
      <Header data={this.state.resumeData.main}/>   
    </div>
  );
}
}

export default App;
