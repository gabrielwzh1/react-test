import React, { Component } from 'react';

class Header extends Component { 

    render() {

        if (this.props.data) {
            var name = this.props.data.name;
            var bio = this.props.data.bio;          
        }

        return (
            <div>
                <h2>Header</h2>
                <h3>{name}</h3>
                <h4>{bio}</h4>
            </div>
        );
    }
}

export default Header;
